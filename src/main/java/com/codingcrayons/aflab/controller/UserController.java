package com.codingcrayons.aflab.controller;

import com.codingcrayons.aflab.model.User;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;


@Named
@RequestScoped
public class UserController {

	private User user;

	public UserController() {
		user = new User();
		user.setFirstName("John");
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUsers() {
		List<User> users = new ArrayList<User>();
		users.add(user);
		return users;
	}
}

