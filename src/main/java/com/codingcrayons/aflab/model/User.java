package com.codingcrayons.aflab.model;

import com.codingcrayons.aspectfaces.annotations.UiCollection;
import com.codingcrayons.aspectfaces.annotations.UiOrder;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class User {

	private static final long serialVersionUID = 2823201870652640940L;
	private String firstName;
	private Date birthDate;
	private boolean confirmed = false;
	private Country country;

	@UiOrder(1)
	@NotNull
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String fistName) {
		this.firstName = fistName;
	}

	@UiOrder(2)
	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	@UiOrder(4)
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date foo) {
		this.birthDate = foo;
	}

	@UiOrder(5)
	@UiCollection(name = "entities.countries")
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
}
